package com.dtlim.sentiatest

import android.app.Application

class SentiaTestApplication: Application() {

    private val bus: RxBus = RxBus()

    fun getRxBus(): RxBus {
        return bus
    }
}