package com.dtlim.sentiatest.propertyid.view.impl

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dtlim.sentiatest.R
import com.dtlim.sentiatest.RxBus
import com.dtlim.sentiatest.SentiaTestApplication
import com.dtlim.sentiatest.propertyid.presenter.PropertyIdPresenter
import com.dtlim.sentiatest.propertyid.presenter.impl.PropertyIdPresenterImpl
import com.dtlim.sentiatest.propertyid.view.PropertyIdView
import kotlinx.android.synthetic.main.fragment_property_id.*

class PropertyIdFragment: Fragment(), PropertyIdView {

    private var presenter: PropertyIdPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.fragment_property_id, container, false);
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initializePresenter()
    }

    private fun initializePresenter() {
        val bus: RxBus = (context!!.applicationContext as SentiaTestApplication).getRxBus()
        if(presenter == null) {
            presenter = PropertyIdPresenterImpl(bus)
        }
        presenter?.bindView(this)
    }

    override fun setId(id: String) {
        propertyIdTextView.text = id
    }

    override fun setTitle(title: String) {
        propertyTitleTextView.text = title
    }

}