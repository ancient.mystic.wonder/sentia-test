package com.dtlim.sentiatest.propertyid.presenter.impl

import android.util.Log
import com.dtlim.sentiatest.RxBus
import com.dtlim.sentiatest.propertyid.presenter.PropertyIdPresenter
import com.dtlim.sentiatest.propertyid.view.PropertyIdView
import com.dtlim.sentiatest.propertylist.adapter.DisplayProperty
import io.reactivex.functions.Consumer

class PropertyIdPresenterImpl(val bus: RxBus): PropertyIdPresenter{

    private lateinit var view: PropertyIdView
    private var displayProperty: DisplayProperty? = null
    private var subscribed: Boolean = false

    override fun bindView(view: PropertyIdView) {
        this.view = view

        if(displayProperty != null) {
            setViewProperty(displayProperty!!)
        }

        if(!subscribed) {
            bus.subscribeToEvent("propertyclick", Consumer { t ->
                Log.d("TEST", "recv " + t.toString())
                if(t is DisplayProperty) {
                    displayProperty = t
                    setViewProperty(t)
                }
            })
            subscribed = true
        }
    }

    private fun setViewProperty(displayProperty: DisplayProperty) {
        view.setId(displayProperty.id.toString())
        view.setTitle(displayProperty.title)
    }
}