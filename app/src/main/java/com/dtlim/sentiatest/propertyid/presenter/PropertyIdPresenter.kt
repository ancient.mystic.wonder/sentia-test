package com.dtlim.sentiatest.propertyid.presenter

import com.dtlim.sentiatest.base.BasePresenter
import com.dtlim.sentiatest.propertyid.view.PropertyIdView

interface PropertyIdPresenter: BasePresenter<PropertyIdView> {
}