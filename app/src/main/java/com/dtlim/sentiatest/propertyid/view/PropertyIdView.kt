package com.dtlim.sentiatest.propertyid.view

import com.dtlim.sentiatest.base.BaseView

interface PropertyIdView: BaseView {
    fun setId(id: String)
    fun setTitle(title: String)
}