package com.dtlim.sentiatest.propertylist.presenter.impl

import android.util.Log
import com.dtlim.sentiatest.RxBus
import com.dtlim.sentiatest.data.PropertyModel
import com.dtlim.sentiatest.data.api.PropertyService
import com.dtlim.sentiatest.data.api.impl.PropertyServiceImpl
import com.dtlim.sentiatest.data.database.PropertyRepository
import com.dtlim.sentiatest.data.database.PropertyRepositoryImpl
import com.dtlim.sentiatest.data.entity.Property
import com.dtlim.sentiatest.propertylist.adapter.DisplayProperty
import com.dtlim.sentiatest.propertylist.presenter.PropertyListPresenter
import com.dtlim.sentiatest.propertylist.view.PropertyListView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PropertyListPresenterImpl(val bus: RxBus): PropertyListPresenter {

    private lateinit var view: PropertyListView
    private var initialized: Boolean = false

    private var model: PropertyModel = PropertyModel(PropertyServiceImpl(), PropertyRepositoryImpl())

    override fun bindView(view: PropertyListView) {
        this.view = view
        model.getStoredProperties()
                .subscribe {
                    setPropertyList(it)
                }

        // Do API call only once
        if(!initialized) {
            view.showLoading()
            model.fetchProperties()
            initialized = true
        }
    }

    private fun setPropertyList(properties: List<Property>) {
        val displayProperties: List<DisplayProperty> = properties.map {
            DisplayProperty(it)
        }
        view.setProperties(displayProperties)
        view.removeRefreshAnimation()
        view.hideLoading()
    }

    override fun onSwipeRefresh() {
        model.fetchProperties()
    }

    override fun onClickProperty(displayProperty: DisplayProperty) {
        Log.d("TEST", "send " + displayProperty.toString())
        bus.publishEvent("propertyclick", displayProperty)
    }

}