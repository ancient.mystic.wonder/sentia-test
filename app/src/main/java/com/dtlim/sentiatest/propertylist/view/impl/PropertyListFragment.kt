package com.dtlim.sentiatest.propertylist.view.impl

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.dtlim.sentiatest.R
import com.dtlim.sentiatest.RxBus
import com.dtlim.sentiatest.SentiaTestApplication
import com.dtlim.sentiatest.propertylist.adapter.DisplayProperty
import com.dtlim.sentiatest.propertylist.adapter.PropertyListAdapter
import com.dtlim.sentiatest.propertylist.presenter.PropertyListPresenter
import com.dtlim.sentiatest.propertylist.presenter.impl.PropertyListPresenterImpl
import com.dtlim.sentiatest.propertylist.view.PropertyListView
import icepick.Icepick
import icepick.State
import kotlinx.android.synthetic.main.fragment_property_list.*

class PropertyListFragment : Fragment(), PropertyListView {

    private lateinit var adapter: PropertyListAdapter
    @State private var presenter: PropertyListPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_property_list, container, false);
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Icepick.restoreInstanceState(this, savedInstanceState)

        initializeRecyclerView()
        initializePresenter()
        initializeSwipeRefresh()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Icepick.saveInstanceState(this, outState)
    }

    private fun initializeRecyclerView() {
        adapter = PropertyListAdapter(context!!, object: PropertyListAdapter.Listener {
            override fun onPropertyItemClicked(displayProperty: DisplayProperty) {
                presenter?.onClickProperty(displayProperty)
            }
        })

        propertyListRecyclerView.adapter = adapter
        propertyListRecyclerView.layoutManager = LinearLayoutManager(context)
    }

    private fun initializePresenter() {
        val bus: RxBus = (context!!.applicationContext as SentiaTestApplication).getRxBus()
        if(presenter == null) {
            presenter = PropertyListPresenterImpl(bus)
        }
        presenter?.bindView(this)
    }

    private fun initializeSwipeRefresh() {
        propertyListSwipeRefreshLayout.setOnRefreshListener {
            presenter!!.onSwipeRefresh()
        }
    }

    override fun setProperties(properties: List<DisplayProperty>) {
        adapter.setPropertyList(properties)
    }

    override fun showMessage(message: String) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

    override fun removeRefreshAnimation() {
        propertyListSwipeRefreshLayout.isRefreshing = false
    }

    override fun showLoading() {
        progressView.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progressView.visibility = View.GONE
    }
}