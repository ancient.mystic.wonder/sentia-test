package com.dtlim.sentiatest.propertylist.presenter

import com.dtlim.sentiatest.base.BasePresenter
import com.dtlim.sentiatest.propertylist.adapter.DisplayProperty
import com.dtlim.sentiatest.propertylist.view.PropertyListView

interface PropertyListPresenter: BasePresenter<PropertyListView> {
    fun onSwipeRefresh()
    fun onClickProperty(displayProperty: DisplayProperty)
}