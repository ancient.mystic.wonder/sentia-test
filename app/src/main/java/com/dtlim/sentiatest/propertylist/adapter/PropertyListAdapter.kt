package com.dtlim.sentiatest.propertylist.adapter

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.dtlim.sentiatest.R
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.view_property_list_item.view.*

class PropertyListAdapter(private var context: Context, private var listener: Listener) : RecyclerView.Adapter<PropertyListViewHolder>() {

    private var propertyList: List<DisplayProperty> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PropertyListViewHolder {
        val layoutInflater: LayoutInflater = LayoutInflater.from(context)
        val view: View = layoutInflater.inflate(R.layout.view_property_list_item, parent, false)

        return PropertyListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return propertyList.size
    }

    override fun onBindViewHolder(holder: PropertyListViewHolder, position: Int) {
        val property: DisplayProperty = propertyList[position]
        holder.propertyTitleTextView.text = property.title
        holder.addressLine1TextView.text = property.addressLine1
        holder.addressLine2TextView.text = property.addressLine2
        holder.nameTextView.text = property.owner
        holder.bedCountTextView.text = property.bedroomCount.toString()
        holder.bathCountTextView.text = property.bathroomCount.toString()
        holder.carCountTextView.text = property.carCount.toString()

        loadPropertyImages(holder, property)
        if(property.isPremium) {
            setPremiumProperty(holder)
        } else {
            setDefaultProperty(holder)
        }

        // add listener
        holder.container.setOnClickListener {
            listener.onPropertyItemClicked(property)
        }
    }

    private fun loadPropertyImages(holder: PropertyListViewHolder, property: DisplayProperty) {
        Picasso.with(context)
                .load(property.propertyPhotoUrl)
                .fit()
                .centerCrop()
                .into(holder.propertyImageView, object: Callback {
                    override fun onSuccess() {

                    }

                    override fun onError() {
                        holder.propertyImageView.visibility = View.GONE
                    }
                })

        Picasso.with(context)
                .load(property.avatarUrl)
                .fit()
                .centerCrop()
                .placeholder(R.drawable.ic_avatar_placeholder)
                .transform(CircleTransform())
                .into(holder.avatarImageView)
    }

    private fun setPremiumProperty(holder: PropertyListViewHolder) {
        holder.container.setCardBackgroundColor(
                ContextCompat.getColor(context, R.color.premiumPropertyBackground))
        holder.premiumTextView.visibility = View.VISIBLE
    }

    private fun setDefaultProperty(holder: PropertyListViewHolder) {
        holder.container.setCardBackgroundColor(
                ContextCompat.getColor(context, R.color.defaultPropertyBackground))
        holder.premiumTextView.visibility = View.GONE
    }

    fun setPropertyList(propertyList: List<DisplayProperty>) {
        this.propertyList = propertyList
        notifyDataSetChanged()
    }

    interface Listener {
        fun onPropertyItemClicked(displayProperty: DisplayProperty)
    }
}

class PropertyListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    val container: CardView = itemView.container
    val propertyImageView: ImageView = itemView.propertyImageView
    val propertyTitleTextView: TextView = itemView.propertyTitleTextView
    val premiumTextView: TextView = itemView.premiumTextView
    val addressLine1TextView: TextView = itemView.addressLine1TextView
    val addressLine2TextView: TextView = itemView.addressLine2TextView
    val avatarImageView: ImageView = itemView.avatarImageView
    val nameTextView: TextView = itemView.nameTextView
    val bedCountTextView: TextView = itemView.bedCountTextView
    val bathCountTextView: TextView = itemView.bathCountTextView
    val carCountTextView: TextView = itemView.carCountTextView
}