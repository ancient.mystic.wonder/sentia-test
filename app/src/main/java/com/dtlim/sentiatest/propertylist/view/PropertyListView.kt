package com.dtlim.sentiatest.propertylist.view

import com.dtlim.sentiatest.base.BaseView
import com.dtlim.sentiatest.propertylist.adapter.DisplayProperty

interface PropertyListView: BaseView {
    fun setProperties(properties: List<DisplayProperty>)
    fun showMessage(message: String)
    fun showLoading()
    fun hideLoading()
    fun removeRefreshAnimation()
}