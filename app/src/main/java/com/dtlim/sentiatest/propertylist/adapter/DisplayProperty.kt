package com.dtlim.sentiatest.propertylist.adapter

import com.dtlim.sentiatest.data.entity.Property
import java.math.BigDecimal

data class DisplayProperty(val id: Long,
                           val title: String,
                           val description: String,
                           val price: BigDecimal,
                           val owner: String,
                           val avatarUrl: String,
                           val addressLine1: String,
                           val addressLine2: String,
                           val suburb: String,
                           val propertyPhotoUrl: String,
                           val postCode: String,
                           val bedroomCount: Int,
                           val bathroomCount: Int,
                           val carCount: Int,
                           val isPremium: Boolean) {

    constructor(property: Property) : this(property.id, property.title, property.description,
            property.price, property.owner.firstName + " " + property.owner.lastName,
            property.owner.avatar.medium.url, property.location.address1, property.location.address2,
            property.location.suburb, property.photo.image.url, property.location.postCode, property.bedroomCount,
            property.bathroomCount, property.carSpots, property.isPremium)

}