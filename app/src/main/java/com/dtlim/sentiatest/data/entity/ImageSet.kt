package com.dtlim.sentiatest.data.entity

import com.google.gson.annotations.SerializedName

data class ImageSet(
        @SerializedName("url") val url: String,
        @SerializedName("small") val small: ImageUrl,
        @SerializedName("medium") val meduim: ImageUrl,
        @SerializedName("large") val large: ImageUrl
)