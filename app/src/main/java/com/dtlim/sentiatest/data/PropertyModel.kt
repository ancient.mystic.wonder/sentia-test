package com.dtlim.sentiatest.data

import com.dtlim.sentiatest.data.api.PropertyService
import com.dtlim.sentiatest.data.database.PropertyRepository
import com.dtlim.sentiatest.data.entity.Property
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PropertyModel(private var service: PropertyService,
                    private var repository: PropertyRepository) {

    fun fetchProperties() {
        service.getPropertiesFromApi()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(this::storeProperties)
    }

    fun getStoredProperties(): Observable<List<Property>> {
        return repository.getAllProperties()
    }

    private fun storeProperties(propertyList: List<Property>) {
        repository.saveProperties(propertyList)
    }

}