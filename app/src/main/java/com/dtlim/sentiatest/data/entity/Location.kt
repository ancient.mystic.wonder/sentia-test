package com.dtlim.sentiatest.data.entity

import com.google.gson.annotations.SerializedName

data class Location(
        @SerializedName("id") val id: Long,
        @SerializedName("address_1") val address1: String,
        @SerializedName("address_2") val address2: String,
        @SerializedName("suburb") val suburb: String,
        @SerializedName("state") val state: String,
        @SerializedName("postcode") val postCode: String,
        @SerializedName("country") val country: String,
        @SerializedName("latitude") val latitude: String,
        @SerializedName("longitude") val longitude: String,
        @SerializedName("full_address") val fullAddress: String
)