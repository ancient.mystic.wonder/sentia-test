package com.dtlim.sentiatest.data.api

import com.dtlim.sentiatest.data.entity.Property
import io.reactivex.Observable

interface PropertyService {
    fun getPropertiesFromApi(): Observable<List<Property>>
}