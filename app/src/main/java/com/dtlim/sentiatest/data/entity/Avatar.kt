package com.dtlim.sentiatest.data.entity

import com.google.gson.annotations.SerializedName

data class Avatar(
        @SerializedName("url") val url: String,
        @SerializedName("small") val small: ImageUrl,
        @SerializedName("medium") val medium: ImageUrl,
        @SerializedName("large") val large: ImageUrl,
        @SerializedName("profile") val profile: ImageUrl
)