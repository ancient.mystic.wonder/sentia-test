package com.dtlim.sentiatest.data.entity

import com.google.gson.annotations.SerializedName

data class Photo(
        @SerializedName("id") val id: Long,
        @SerializedName("default") val default: Boolean,
        @SerializedName("image") val image: ImageSet
)