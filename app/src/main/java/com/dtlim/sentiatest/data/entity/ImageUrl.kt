package com.dtlim.sentiatest.data.entity

import com.google.gson.annotations.SerializedName

data class ImageUrl(@SerializedName("url") val url: String)