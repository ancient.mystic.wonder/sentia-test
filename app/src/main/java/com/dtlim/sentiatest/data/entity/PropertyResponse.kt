package com.dtlim.sentiatest.data.entity

import com.google.gson.annotations.SerializedName

data class PropertyResponse(@SerializedName("data") val data: List<Property>)