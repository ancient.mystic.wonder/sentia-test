package com.dtlim.sentiatest.data.api.impl

import android.util.Log
import com.dtlim.sentiatest.data.api.PropertyInterface
import com.dtlim.sentiatest.data.api.PropertyService
import com.dtlim.sentiatest.data.entity.Property
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class PropertyServiceImpl: PropertyService {

    private final val TAG: String = PropertyServiceImpl::class.java.simpleName

    private var requestInterface: PropertyInterface = Retrofit.Builder()
            .baseUrl("http://demo0065087.mockable.io")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(PropertyInterface::class.java)

    override fun getPropertiesFromApi(): Observable<List<Property>> {
        Log.d(TAG, "Performing API GET")
        return requestInterface.getProperties().flatMap {
            Observable.just(it.data)
        }
    }
}