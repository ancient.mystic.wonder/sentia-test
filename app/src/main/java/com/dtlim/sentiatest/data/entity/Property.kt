package com.dtlim.sentiatest.data.entity

import com.google.gson.annotations.SerializedName
import java.math.BigDecimal

data class Property(
        @SerializedName("id") val id: Long,
        @SerializedName("is_premium") val isPremium: Boolean,
        @SerializedName("state") val state: String,
        @SerializedName("title") val title: String,
        @SerializedName("bedrooms") val bedroomCount: Int,
        @SerializedName("bathrooms") val bathroomCount: Int,
        @SerializedName("carspots") val carSpots: Int,
        @SerializedName("description") val description: String,
        @SerializedName("price") val price: BigDecimal,
        @SerializedName("owner") val owner: Owner,
        @SerializedName("location") val location: Location,
        @SerializedName("photo") val photo: Photo
)