package com.dtlim.sentiatest.data.api

import com.dtlim.sentiatest.data.entity.Property
import com.dtlim.sentiatest.data.entity.PropertyResponse
import io.reactivex.Observable
import retrofit2.http.GET

interface PropertyInterface {
    @GET("/test/properties")
    fun getProperties(): Observable<PropertyResponse>
}