package com.dtlim.sentiatest.data.entity

import com.google.gson.annotations.SerializedName

data class Owner(
        @SerializedName("id") val id: Long,
        @SerializedName("first_name") val firstName: String,
        @SerializedName("last_name") val lastName: String,
        @SerializedName("email") val email: String,
        @SerializedName("avatar") val avatar: Avatar
)