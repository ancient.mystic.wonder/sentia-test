package com.dtlim.sentiatest.data.database

import com.dtlim.sentiatest.data.entity.Property
import io.reactivex.Observable

interface PropertyRepository {
    fun getAllProperties(): Observable<List<Property>>
    fun saveProperties(properties: List<Property>): Boolean
}