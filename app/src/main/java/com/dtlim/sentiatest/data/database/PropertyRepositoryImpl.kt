package com.dtlim.sentiatest.data.database

import com.dtlim.sentiatest.data.entity.Property
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject

// TODO database
class PropertyRepositoryImpl: PropertyRepository {

    private var subject: BehaviorSubject<List<Property>> = BehaviorSubject.create()
    private var propertyList: List<Property> = ArrayList() // soft cache, will move to database

    override fun getAllProperties(): Observable<List<Property>> {
        return subject
    }

    override fun saveProperties(properties: List<Property>): Boolean {
        propertyList = properties
        subject.onNext(propertyList)

        return true
    }
}