package com.dtlim.sentiatest.base

interface BasePresenter<in T : BaseView> {
    fun bindView(view : T)
}