package com.dtlim.sentiatest

import io.reactivex.functions.Consumer
import io.reactivex.subjects.PublishSubject

class RxBus {
    private var subjects: MutableMap<String, PublishSubject<Any>> = HashMap()

    fun subscribeToEvent(event: String, observable: Consumer<Any>) {
        if(subjects[event] == null) {
            subjects[event] = PublishSubject.create()
        }
        subjects[event]!!.subscribe(observable)
    }

    fun publishEvent(event: String, payload: Any) {
        if(subjects[event] == null) {
            subjects[event] = PublishSubject.create()
        }
        subjects[event]!!.onNext(payload)
    }
}