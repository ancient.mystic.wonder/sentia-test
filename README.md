# Sentia Test App

Here is my submission for the Sentia Test app. Note that I included the property title in for the right pane fragment because the all the properties returned by the API have an id of '1'. This app is architectured using mostly MVP.

### Structure
The `data` package contains the classes used to obtain the data. The `PropertyModel`'s role is to both fetch objects from the API and save them locally. It does so via the `PropertyService` & `PropertyRepository` objects stored at the `api` and `database` packages respectively.

The idea here is all presenters will only subscribe to the repository (via SQLBrite or some observable database). Visiting the page for the first time or swipe refreshing will trigger the model to fetch from the API. After the data is fetched, the model will update repository, which will trigger the onNext for any subscribed presenter.

Note: for now the repository is implemented as a `BehaviorSubject` with a local variable for storing the property. Ideally it would be a reactive database such as SQLBrite.

The `propertylist` and `propertyid` packages possess the views and presenters. The fragments are used as the views. I used the `Icepick` library to preserve the presenter's state on device rotate.

Also, I implemented an mini Rx event bus that can filter events by keys at `RxBus.kt` (not sure if this is efficient and/or correct, haven't tested it thoroughly).  I use this bus for inter-fragment communication. Once a list item is clicked, the presenter sends an event of key "propertyclick" to the bus. The bus then emits the event to all other presenters subscribed to the "propertyclick" event via the bus.

Some things that are still missing (will add if I have time):
  - Dagger 2 setup
  - Database (SQLBrite)
  - material animations
  - unit tests